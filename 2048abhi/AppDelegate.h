//
//  AppDelegate.h
//  2048abhi
//
//  Created by Abhishek Gupta on 1/22/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

