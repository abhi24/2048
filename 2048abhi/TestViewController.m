//
//  TestViewController.m
//  2048abhi
//
//  Created by Abhishek Gupta on 1/28/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import "TestViewController.h"
#import "SWRevealViewController.h"
@interface TestViewController ()

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _barButton.target =self.revealViewController;
    _barButton.action =@selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
