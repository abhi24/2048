//
//  ViewController.m
//  2048abhi
//
//  Created by Abhishek Gupta on 1/22/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

#define FrameNumber CGRectMake(120, CGRectGetMidY([[self view] frame])+135, 180, 220)

@synthesize goup, godown,moveright,moveleft,highestScore,currentScore,resetButton,lblarray ,finish;
NSUserDefaults *scores;

int finalscore =0;

-(void)RandomNumber{
    
    NSInteger random = arc4random_uniform(16);
    if([[[lblarray objectAtIndexedSubscript:random] text] isEqualToString:@""])
    {
        [[lblarray objectAtIndexedSubscript:random] setText: @"4"];
    }
    
    NSInteger randomer = arc4random_uniform(16);
    if(randomer==random)
    {
        randomer = arc4random_uniform(16);
    }
    if([[[lblarray objectAtIndexedSubscript:randomer] text] isEqualToString:@""])
    {
        [[lblarray objectAtIndexedSubscript:randomer] setText: @"4"];
    }
}

-(void)generateRandomNumber{
    NSInteger number = arc4random_uniform(16);
    if([[[lblarray objectAtIndexedSubscript:number]text]isEqualToString:@""])
    {
        [[lblarray objectAtIndexedSubscript:number]setText:@"4"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *btnsound=    [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"beep18" ofType:@"mp3"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)btnsound, &SoundID);

    NSURL *btnsound1=    [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"gameOverSound" ofType:@"wav"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)btnsound1, &SoundID1);
    
//    self.currentScore.alpha=0;
//    
//    [UIView animateWithDuration:0.5
//                          delay:1.0
//                        options: UIViewAnimationOptionRepeat
//                     animations:^{
//                         self.currentScore.alpha=1;
//                     }
//                     completion:^(BOOL finished){
//                         NSLog(@"Done!");
//                     }];
//}
    
//    [UIView animateWithDuration:0.5
//                          delay:0.5
//                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
//                     animations:^{
//                         self.currentScore.alpha=1;}];}
//        

    
    scores = [NSUserDefaults standardUserDefaults];
    [scores synchronize];
    
    NSInteger theHighScore = [scores integerForKey:@"HighScore"];
    highestScore.text= [NSString stringWithFormat:@"%ld", (long)theHighScore];
    
    
    
    [self RandomNumber];
}

-(void)gamecompletehogyi
{
    for (int re=0; re<16; re++)
    {
        if([[[lblarray objectAtIndexedSubscript:re] text] isEqual:@"2048"])
        {
            
            AudioServicesPlaySystemSound(SoundID1);

            UIImageView *background = [[UIImageView alloc] initWithFrame:[[self view] bounds]];
            [background setImage:[UIImage imageNamed:@"Box.jpg"]];
            [background setContentMode:UIViewContentModeScaleAspectFit];
            [[self view] addSubview:background];
            
            
//            
            UIImageView *animator = [[UIImageView alloc] initWithFrame:FrameNumber];
            [animator setImage:[UIImage imageNamed:@"Black.jpg"]];
            [animator setContentMode:UIViewContentModeScaleAspectFit];
            [[self view] addSubview:animator];
        
        }
        
    }
    
}
-(void)GameCompleted
{
    int checker = 0;
    Boolean jack = false;;
    int gamecomplete=0;
    for (gamecomplete=0; gamecomplete<12; gamecomplete++)
    {
        if([[[lblarray objectAtIndexedSubscript:gamecomplete] text] isEqualToString:@"2048"])
        {
            finish.text= @"Congratulations ! ! ! ! :)";
            finish.hidden=NO;
            godown.enabled=NO;
            godown.hidden=YES;
            goup.enabled=NO;
            moveright.enabled=NO;
            moveleft.enabled=NO;
            
        }
        checker++;
        if([[[lblarray objectAtIndexedSubscript:gamecomplete] text] isEqualToString:@""])
        {
            jack = true;
        }
        else
        {
            if((checker%4) == 0)
            {
                if([[[lblarray objectAtIndexedSubscript:gamecomplete] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(gamecomplete+4)] text]])
                {
                    jack = true;
                }
            }
            else
            {
                if([[[lblarray objectAtIndexedSubscript:gamecomplete] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(gamecomplete+1)] text]])
                {
                    jack = true;
                }
                else if([[[lblarray objectAtIndexedSubscript:gamecomplete] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(gamecomplete+4)] text]])
                {
                    jack = true;
                }
            }
            
            if(gamecomplete == 11)
            {
                if([[[lblarray objectAtIndexedSubscript:12] text] isEqualToString:[[lblarray objectAtIndexedSubscript:13] text]])
                {
                    jack = true;
                }
                else if([[[lblarray objectAtIndexedSubscript:13] text] isEqualToString:[[lblarray objectAtIndexedSubscript:14] text]])
                {
                    jack = true;
                }
                else if([[[lblarray objectAtIndexedSubscript:14] text] isEqualToString:[[lblarray objectAtIndexedSubscript:15] text]])
                {
                    jack = true;
                }
            }
            
        }
        
        if(jack)
            break;
    }
    if(jack==false)
    {
        UIImageView *background = [[UIImageView alloc] initWithFrame:[[self view] bounds]];
        [background setImage:[UIImage imageNamed:@"Box.jpg"]];
        [background setContentMode:UIViewContentModeScaleAspectFit];
        [[self view] addSubview:background];
        
        finish.hidden=NO;
        godown.enabled=NO;
        godown.hidden=TRUE;
        
        goup.enabled=NO;
        moveright.enabled=NO;
        moveleft.enabled=NO;
    }
    else
    {
        [self generateRandomNumber];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)goup:(id)sender {
    
    AudioServicesPlaySystemSound(SoundID);
    
    [self upbutton];
    [self addresultoupadd];
    [self gamecompletehogyi];
    [self upbutton];
    [self GameCompleted];
    
}
-(void)upbutton
{
    int upcall=0;
    
    for (upcall=0; upcall<4; upcall++)
    {
        if([[[lblarray objectAtIndexedSubscript:upcall] text] isEqualToString:@""])
        {
            [[lblarray objectAtIndexedSubscript:upcall] setText: [[lblarray objectAtIndexedSubscript:(upcall+4)] text]];
            [[lblarray objectAtIndexedSubscript:(upcall+4)] setText: @""];
        }
        if([[[lblarray objectAtIndexedSubscript:upcall+4] text] isEqualToString:@""])
        {
            if([[[lblarray objectAtIndexedSubscript:upcall] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:upcall] setText: [[lblarray objectAtIndexedSubscript:(upcall+8)] text]];
                [[lblarray objectAtIndexedSubscript:(upcall+8)] setText: @""];
            }
            else
            {
                [[lblarray objectAtIndexedSubscript:upcall+4] setText: [[lblarray objectAtIndexedSubscript:(upcall+8)] text]];
                [[lblarray objectAtIndexedSubscript:(upcall+8)] setText: @""];
            }
        }
        
        if([[[lblarray objectAtIndexedSubscript:upcall+8] text] isEqualToString:@""])
        {
            if([[[lblarray objectAtIndexedSubscript:upcall] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:upcall] setText: [[lblarray objectAtIndexedSubscript:(upcall+12)] text]];
                [[lblarray objectAtIndexedSubscript:(upcall+12)] setText: @""];
                
            }
            else if([[[lblarray objectAtIndexedSubscript:upcall+4] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:upcall+4] setText: [[lblarray objectAtIndexedSubscript:(upcall+12)] text]];
                [[lblarray objectAtIndexedSubscript:(upcall+12)] setText: @""];
            }
            else
            {
                [[lblarray objectAtIndexedSubscript:upcall+8] setText: [[lblarray objectAtIndexedSubscript:(upcall+12)] text]];
                [[lblarray objectAtIndexedSubscript:(upcall+12)] setText: @""];
            }
        }
    }
    
}
-(void)addresultoupadd
{
    int k = 0;
    while(k<12)
    {
        if ([[[lblarray objectAtIndexedSubscript:k] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(k+4)] text]]) {
            
            
            int a = [[[lblarray objectAtIndexedSubscript:k] text] intValue];
            int b = [[[lblarray objectAtIndexedSubscript:(k+4)] text] intValue];
            int final = a + b;
            if(final > 0)
            {
                finalscore=finalscore+final;
                [[lblarray objectAtIndexedSubscript:k] setText :[NSString stringWithFormat:@"%d",final] ];
                [[lblarray objectAtIndexedSubscript:(k+4)] setText:@""];
            }
        }
        k=k+1;
        currentScore.text= [NSString stringWithFormat:@"%d", finalscore];
        [self validatetoppoints];
        
    }
    
}

- (IBAction)godown:(id)sender {
    
    AudioServicesPlaySystemSound(SoundID);
    
    [self downbutton];
    [self addresultodownadd];
    
    [self gamecompletehogyi];
    
    [self downbutton];
    [self GameCompleted];
}
-(void) downbutton
{
    for(int DOWNCALL = 0;DOWNCALL < 4 ; DOWNCALL++)
    {
        if ([[[lblarray objectAtIndexedSubscript:(DOWNCALL+12)] text] isEqualToString:@""])
        {
            [[lblarray objectAtIndexedSubscript:(DOWNCALL+12)] setText:[[lblarray objectAtIndexedSubscript:(DOWNCALL+8)] text]];
            [[lblarray objectAtIndexedSubscript:(DOWNCALL+8)] setText:@""];
        }
        
        if([[[lblarray objectAtIndexedSubscript:(DOWNCALL+8)] text] isEqualToString:@""])
        {
            if ([[[lblarray objectAtIndexedSubscript:(DOWNCALL+12)] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:(DOWNCALL+12)] setText:[[lblarray objectAtIndexedSubscript:(DOWNCALL+4)] text]];
                [[lblarray objectAtIndexedSubscript:(DOWNCALL+4)] setText:@""];
            }
            else
            {
                [[lblarray objectAtIndexedSubscript:(DOWNCALL+8)] setText:[[lblarray objectAtIndexedSubscript:(DOWNCALL+4)] text]];
                [[lblarray objectAtIndexedSubscript:(DOWNCALL+4)] setText:@""];
            }
        }
        if([[[lblarray objectAtIndexedSubscript:(DOWNCALL+4)] text] isEqualToString:@""])
        {
            if ([[[lblarray objectAtIndexedSubscript:(DOWNCALL+12)] text] isEqualToString:@""]) {
                [[lblarray objectAtIndexedSubscript:(DOWNCALL+12)] setText:[[lblarray objectAtIndexedSubscript:(DOWNCALL)] text]];
                [[lblarray objectAtIndexedSubscript:(DOWNCALL)] setText:@""];
                
            }
            else if ([[[lblarray objectAtIndexedSubscript:(DOWNCALL+8)] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:(DOWNCALL+8)] setText:[[lblarray objectAtIndexedSubscript:(DOWNCALL)] text]];
                [[lblarray objectAtIndexedSubscript:(DOWNCALL)] setText:@""];
                
            }
            else
            {
                [[lblarray objectAtIndexedSubscript:(DOWNCALL+4)] setText:[[lblarray objectAtIndexedSubscript:(DOWNCALL)] text]];
                [[lblarray objectAtIndexedSubscript:(DOWNCALL)] setText:@""];
                
                
            }
        }
    }
    
    
    
}
-(void) addresultodownadd
{
    int k = 15;
    while(k>=4)
    {
        if ([[[lblarray objectAtIndexedSubscript:k] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(k-4)] text]])
        {
            int a = [[[lblarray objectAtIndexedSubscript:k] text] intValue];
            int b = [[[lblarray objectAtIndexedSubscript:(k-4)] text] intValue];
            int final = a + b;
            
            if(final > 0)
            {
                finalscore=finalscore+final;
                
                [[lblarray objectAtIndexedSubscript:k] setText :[NSString stringWithFormat:@"%d",final] ];
                
                [[lblarray objectAtIndexedSubscript:(k-4)] setText:@""];
                
            }
        }
        k=k-1;
        currentScore.text= [NSString stringWithFormat:@"%d", finalscore];
        [self validatetoppoints];
        
    }
}
- (IBAction)moveright:(id)sender {
    
    AudioServicesPlaySystemSound(SoundID);
    
    [self rightbutton];
    [self addresultofrightadd];
    [self gamecompletehogyi];
    [self rightbutton];
    [self GameCompleted];
}
-(void)rightbutton
{
    int rightcall=0;
    while(rightcall<=12) {
        if([[[lblarray objectAtIndexedSubscript:(rightcall+3)] text] isEqualToString:@""])
        {
            [[lblarray objectAtIndexedSubscript:(rightcall+3)] setText: [[lblarray objectAtIndexedSubscript:(rightcall+2)] text]];
            [[lblarray objectAtIndexedSubscript:(rightcall+2)] setText: @""];
        }
        
        if([[[lblarray objectAtIndexedSubscript:(rightcall+2)] text] isEqualToString:@""])
        {
            if([[[lblarray objectAtIndexedSubscript:(rightcall+3)] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:(rightcall+3)] setText: [[lblarray objectAtIndexedSubscript:(rightcall+1)] text]];
                [[lblarray objectAtIndexedSubscript:(rightcall+1)] setText: @""];
                
            }
            else
            {
                
                [[lblarray objectAtIndexedSubscript:(rightcall+2)] setText: [[lblarray objectAtIndexedSubscript:(rightcall+1)] text]];
                [[lblarray objectAtIndexedSubscript:(rightcall+1)] setText: @""];
            }
        }
        
        if([[[lblarray objectAtIndexedSubscript:(rightcall+1)] text] isEqualToString:@""])
        {
            if([[[lblarray objectAtIndexedSubscript:rightcall+3] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:(rightcall+3)] setText: [[lblarray objectAtIndexedSubscript:(rightcall)] text]];
                [[lblarray objectAtIndexedSubscript:(rightcall)] setText: @""];
                
            }
            else if([[[lblarray objectAtIndexedSubscript:rightcall+2] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:(rightcall+2)] setText: [[lblarray objectAtIndexedSubscript:(rightcall)] text]];
                [[lblarray objectAtIndexedSubscript:(rightcall)] setText: @""];
            }
            else
            {
                [[lblarray objectAtIndexedSubscript:(rightcall+1)] setText: [[lblarray objectAtIndexedSubscript:(rightcall)] text]];
                [[lblarray objectAtIndexedSubscript:(rightcall)] setText: @""];
            }
        }
        rightcall=rightcall+4;
    }
    
}
-(void)addresultofrightadd
{
    int k = 0;
    while(k<=12)
    {
        if ([[[lblarray objectAtIndexedSubscript:(k+3)] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(k+2)] text]]) {
            int m = [[[lblarray objectAtIndexedSubscript:k+3] text] intValue];
            int n = [[[lblarray objectAtIndexedSubscript:(k+2)] text] intValue];
            int final = m + n;
            if(final > 0)
            {
                finalscore+=final;
                [[lblarray objectAtIndexedSubscript:k+3] setText :[NSString stringWithFormat:@"%d",final] ];
                [[lblarray objectAtIndexedSubscript:(k+2)] setText:@""];
            }
        }
        if ([[[lblarray objectAtIndexedSubscript:(k+2)] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(k+1)] text]]) {
            int m = [[[lblarray objectAtIndexedSubscript:(k+2)] text] intValue];
            int n = [[[lblarray objectAtIndexedSubscript:(k+1)] text] intValue];
            int final = m + n;
            if(final > 0)
            {
                finalscore=finalscore+final;
                [[lblarray objectAtIndexedSubscript:(k+2)] setText :[NSString stringWithFormat:@"%d",final] ];
                [[lblarray objectAtIndexedSubscript:(k+1)] setText:@""];
            }
        }
        if ([[[lblarray objectAtIndexedSubscript:(k+1)] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(k)] text]]) {
            int m = [[[lblarray objectAtIndexedSubscript:(k+1)] text] intValue];
            int n = [[[lblarray objectAtIndexedSubscript:(k)] text] intValue];
            int final = m + n;
            if(final > 0)
            {
                finalscore=finalscore+final;
                [[lblarray objectAtIndexedSubscript:(k+1)] setText :[NSString stringWithFormat:@"%d",final] ];
                [[lblarray objectAtIndexedSubscript:(k)] setText:@""];
            }
        }
        k=k+4;
        currentScore.text= [NSString stringWithFormat:@"%d", finalscore];
        [self validatetoppoints];
        
    }
}

- (IBAction)moveleft:(id)sender
{
    AudioServicesPlaySystemSound(SoundID);
  
    
    [self leftbutton];
    [self addresultofleftadd];
    [self gamecompletehogyi];
    [self leftbutton];
    [self GameCompleted];
}
-(void) leftbutton
{
    int j=0;
    while(j<=12) {
        if([[[lblarray objectAtIndexedSubscript:j] text] isEqualToString:@""])
        {
            [[lblarray objectAtIndexedSubscript:j] setText: [[lblarray objectAtIndexedSubscript:(j+1)] text]];
            [[lblarray objectAtIndexedSubscript:(j+1)] setText: @""];
        }
        
        if([[[lblarray objectAtIndexedSubscript:j+1] text] isEqualToString:@""])
        {
            if([[[lblarray objectAtIndexedSubscript:j] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:j] setText: [[lblarray objectAtIndexedSubscript:(j+2)] text]];
                [[lblarray objectAtIndexedSubscript:(j+2)] setText: @""];
                
            }
            else
            {
                
                [[lblarray objectAtIndexedSubscript:j+1] setText: [[lblarray objectAtIndexedSubscript:(j+2)] text]];
                [[lblarray objectAtIndexedSubscript:(j+2)] setText: @""];
            }
        }
        
        if([[[lblarray objectAtIndexedSubscript:j+2] text] isEqualToString:@""])
        {
            if([[[lblarray objectAtIndexedSubscript:j] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:j] setText: [[lblarray objectAtIndexedSubscript:(j+3)] text]];
                [[lblarray objectAtIndexedSubscript:(j+3)] setText: @""];
                
            }
            else if([[[lblarray objectAtIndexedSubscript:j+1] text] isEqualToString:@""])
            {
                [[lblarray objectAtIndexedSubscript:j+1] setText: [[lblarray objectAtIndexedSubscript:(j+3)] text]];
                [[lblarray objectAtIndexedSubscript:(j+3)] setText: @""];
            }
            else
            {
                [[lblarray objectAtIndexedSubscript:j+2] setText: [[lblarray objectAtIndexedSubscript:(j+3)] text]];
                [[lblarray objectAtIndexedSubscript:(j+3)] setText: @""];
            }
        }
        j=j+4;
    }
    
}
-(void)addresultofleftadd
{
    int k = 0;
    while(k<=12)
    {
        if ([[[lblarray objectAtIndexedSubscript:k] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(k+1)] text]]) {
            int a = [[[lblarray objectAtIndexedSubscript:k] text] intValue];
            int b = [[[lblarray objectAtIndexedSubscript:(k+1)] text] intValue];
            int final = a + b;
            if(final > 0)
            {
                finalscore=finalscore+final;
                [[lblarray objectAtIndexedSubscript:k] setText :[NSString stringWithFormat:@"%d",final] ];
                [[lblarray objectAtIndexedSubscript:(k+1)] setText:@""];
            }
        }
        if ([[[lblarray objectAtIndexedSubscript:(k+1)] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(k+2)] text]]) {
            int a = [[[lblarray objectAtIndexedSubscript:(k+1)] text] intValue];
            int b = [[[lblarray objectAtIndexedSubscript:(k+2)] text] intValue];
            int final = a + b;
            if(final > 0)
            {
                finalscore=finalscore+final;
                [[lblarray objectAtIndexedSubscript:(k+1)] setText :[NSString stringWithFormat:@"%d",final] ];
                [[lblarray objectAtIndexedSubscript:(k+2)] setText:@""];
            }
        }
        if ([[[lblarray objectAtIndexedSubscript:(k+2)] text] isEqualToString:[[lblarray objectAtIndexedSubscript:(k+3)] text]]) {
            int m = [[[lblarray objectAtIndexedSubscript:(k+2)] text] intValue];
            int n = [[[lblarray objectAtIndexedSubscript:(k+3)] text] intValue];
            int final = m + n;
            if(final > 0)
            {
                finalscore=finalscore+final;
                [[lblarray objectAtIndexedSubscript:(k+2)] setText :[NSString stringWithFormat:@"%d",final]];
                [[lblarray objectAtIndexedSubscript:(k+3)] setText:@""];
            }
        }
        k=k+4;
        currentScore.text= [NSString stringWithFormat:@"%d", finalscore];
        [self validatetoppoints];
        
    }
    
}
- (IBAction)resetScore:(id)sender {
    for(int i=0;i<16;i++)
    {
        [[lblarray objectAtIndexedSubscript:i] setText:@""];
    }
    currentScore.text= @"0";
    [self RandomNumber];
    finish.hidden=YES;
}
-(void)validatetoppoints{
    NSInteger theHighScore = [scores integerForKey:@"HighScore"];
    if(finalscore>theHighScore)
    {
        [scores setInteger:finalscore forKey:@"HighScore"];
        highestScore.text= [NSString stringWithFormat:@"%ld", (long)finalscore];
    }
    
}

@end
