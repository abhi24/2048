//
//  PhotoViewController.m
//  2048abhi
//
//  Created by Abhishek Gupta on 1/29/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import "PhotoViewController.h"
#import "SWRevealViewController.h"


@interface PhotoViewController ()

@end

@implementation PhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _barButton.target =self.revealViewController;
    _barButton.action =@selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
